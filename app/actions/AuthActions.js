'use strict';
import axios from 'axios';
import { push } from 'react-router-redux';
import socket from 'socket'

export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const LOGOUT = 'LOGOUT';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const REMOVE_ERROR = 'REMOVE_ERROR';

export function login (credentials) {
  return (dispatch) => {
    axios.post('/api/auth/login', credentials)
      .then((res) => {
        dispatch(authSuccess(res.data.user));
        dispatch(push('/'));
        socket.emit('login', res.data.user._id);
      })
      .catch((res) => {
        dispatch(authFail(res.data.message))
      });
  }
}

export function register (credentials) {
  return (dispatch) => {
    axios.post('/api/auth/register', credentials)
      .then((res) => {
        dispatch(authSuccess(res.data.user));
        dispatch(push('/'));
        socket.emit('login', res.data.user._id);
      })
      .catch((res) => {
        dispatch(authFail(res.data.message))
      });
  }
}

export function authSuccess (user) {
  return {type: AUTH_SUCCESS, user}
}

export function authFail (error) {
  return {type: AUTH_FAIL, error}
}

export function logout () {
  return (dispatch) => {
    axios.get('/api/auth/logout')
      .then(() => {
        dispatch(logoutSuccess());
        dispatch(push('/login'));
        socket.emit('logout');
      })
  }
}

export function logoutSuccess () {
  return {type: LOGOUT_SUCCESS}
}

export function removeError () {
  return {type: REMOVE_ERROR}
}
