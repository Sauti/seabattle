'use strict';
import socket from 'socket';
import { push } from 'react-router-redux';

export const SEND_INVITATION = 'SEND_INVITATION';
export const GET_INVITATION = 'GET_INVITATION';
export const REJECT_INVITATION = 'REJECT_INVITATION';
export const ACCEPT_INVITATION = 'ACCEPT_INVITATION';
export const GET_REJECTION = 'GET_REJECTION';
export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION';
export const CLOSE_NOTIFICATION = 'CLOSE_NOTIFICATION';
export const HANDLE_GAME_START = 'HANDLE_GAME_START';
export const INIT_FIELDS = 'INIT_FIELDS';
export const SHOOT = 'SHOOT';
export const HANDLE_SHOT = 'HANDLE_SHOT';
export const END_GAME = 'END_GAME';

export function sendInvitation (payload) {
  socket.emit('send invitation', payload);

  return {type: SEND_INVITATION};
}

export function getInvitation (id) {
  return (dispatch, getState) => {
    const { game } = getState();
    game.id
      ? dispatch(rejectInvitation({sender: id}))
      : dispatch({type: GET_INVITATION, id});

    let timeout = setTimeout(() => {
        getState().notification.message
          ? dispatch(rejectInvitation({sender: id}))
          : clearTimeout(timeout);
    }, 10000)
  }
}

export function rejectInvitation (payload) {
  socket.emit('reject invitation', payload.sender);

  return {type: REJECT_INVITATION};
}

export function acceptInvitation (payload) {
  socket.emit('accept invitation', payload);

  return {type: ACCEPT_INVITATION};
}

export function getRejection () {
  return {type: GET_REJECTION}
}

export function showNotification (payload) {
  return {type: SHOW_NOTIFICATION, ...payload }
}

export function closeNotification () {
  return {type: CLOSE_NOTIFICATION}
}

export function closeNotificationAndRedirect () {
  return dispatch => {
    dispatch(closeNotification());
    dispatch(push('/'));
  }
}

export function handleGameStart (payload) {
  return dispatch => {
    dispatch(closeNotification());
    dispatch({type: HANDLE_GAME_START, ...payload});
    dispatch(push('/game/' + payload.room));
  }
}

export function initFields (fields) {
  return {type: INIT_FIELDS, fields}
}

export function shoot (payload) {
  socket.emit('shoot', payload);

  return {type: SHOOT, payload}
}

export function handleShot (payload) {
  return (dispatch, getState) => {
    const { game: { myField, opponentField }, auth: { user }} = getState();
    const { turn, targetUser, cells, winner } = payload;

    targetUser === user._id
      ? dispatch({ type: HANDLE_SHOT, turn, myField: updateField(myField, cells) })
      : dispatch({ type: HANDLE_SHOT, turn, opponentField: updateField(opponentField, cells) });

    if (winner) {
      winner === user._id
        ? dispatch(showNotification({message: 'You won.', handler: closeNotificationAndRedirect}))
        : dispatch(showNotification({message: 'You lost.', handler: closeNotificationAndRedirect}))
    }
  }
}

export function endGame () {
  return {type: END_GAME}
}

function updateField (field, cells) {
  let _field = field.map(row => row.slice());
  cells.forEach(cell => _field[cell.y][cell.x] = cell.type);

  return _field;
}
