'use strict';
import axios from 'axios';

export const GET_USERS = 'GET_USERS';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAIL = 'GET_USERS_FAIL';

export function getUsers () {
  return (dispatch) => {
    axios.get('/api/users')
      .then((res) => {
        dispatch(getUsersSuccess(res.data.users));
      })
      .catch((res) => {
        dispatch(getUsersFail(res.data.error))
      });
  }
}

export function getUsersSuccess (users) {
  return {type: GET_USERS_SUCCESS, users}
}

export function getUsersFail (error) {
  return {type: GET_USERS_SUCCESS, error}
}
