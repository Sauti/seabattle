'use strict';
import React, { Component, PropTypes } from 'react';
import style from 'styles/components/field';
import shoot from 'actions/GameActions';

const classNames = {
  "0": 'empty',
  ".": 'wrapper',
  "1": 'ship',
  "*": 'hit',
  "-": 'miss'
};

export default class Field extends Component {
  render () {
    let { grid, onCellClick } = this.props;
    const cursorStyle = {cursor: 'pointer'};

    const cells = grid.map((row, y) => {
      return row.map((cell, x) => {
        return (onCellClick && cell == 0)
          ? <div className={classNames[cell]} data-x={x} data-y={y} onClick={onCellClick} style={cursorStyle}></div>
          : <div className={classNames[cell]} data-x={x} data-y={y}></div>
      });
    });

    return (
      <div className="field col-md-4 col-sm-5 col-xs-6"> {cells} </div>
    );
  }
}

Field.propTypes = {
  grid: PropTypes.array.isRequired,
  onCellClick: PropTypes.func
};
