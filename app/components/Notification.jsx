'use strict';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { rejectInvitation, acceptInvitation } from 'actions/GameActions'

class Notification extends Component {
  render() {
    const { notification, dispatch, currentUser } = this.props;
    const modalStyle = notification.message
      ? {display: "block"}
      : {display: "none"};
    const payload = {
      sender: notification.sender,
      receiver: currentUser._id
    };

    const buttons = notification.onClickHandler.map((action, i) => {
      return <button className="btn btn-default" key={i}
              onClick={() => dispatch(action.handler(payload))}> {action.btnText} </button>
    });

    return (
      <div className="modal fade in" style={modalStyle}>
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
              {notification.message}
            </div>
            <div className="modal-footer">
              {buttons}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    notification: state.notification,
    currentUser: state.auth.user
  };
}

export default connect(mapStateToProps)(Notification);
