'use strict';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import defaultAvatar from 'images/default-avatar.png'
import style from 'styles/components/profile'
import { logout } from 'actions/AuthActions'

class UserInfo extends Component {
  render () {
    const { auth: { user }, dispatch } = this.props;

    return (
      <div className="col-md-3 col-sm-4 col-xs-12 panel panel-default profile">
        <div className="panel-body">
          <div className="avatar-container">
            <img src={defaultAvatar} className="img-responsive thumbnail avatar"></img>
            <strong className="username"> {user.username} </strong>
          </div>
          <div className="stats-container">
            <div>
              <strong>Games played:</strong> <span> {user.games} </span>
            </div>
            <div className="stats-details">
              <div>
                <strong>Won:</strong> <span> {user.win} </span>
              </div>
              <div>
                <strong>Lost:</strong> <span> {user.loose} </span>
              </div>
            </div>
          </div>
          <div className="nav-container">
            <hr />
            <nav className="nav">
              <li><Link to="/" className="active">Users</Link></li>
              <li><Link to="/login" onClick={() => dispatch(logout())}>Logout</Link></li>
            </nav>
            <hr className="hide-xs"/>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

export default connect(mapStateToProps)(UserInfo);
