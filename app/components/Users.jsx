'use strict';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getUsers } from 'actions/UserActions'
import { sendInvitation } from 'actions/GameActions'
import style from 'styles/components/users'
import Notification from 'components/Notification'

class Users extends Component {
  componentWillMount() {
    this.props.dispatch(getUsers());
  }

  render() {
    const { dispatch, currentUser, users, notification } = this.props;
    const list = users.map(user => {
      const payload = {sender: currentUser._id, receiver: user._id};
      const isOnline = user.isOnline ? 'online': 'offline';
      const isBtnDisabled = notification.isWaitingForResponse || !user.isOnline;

      return (
        <tr key={user._id}>
          <td>
            <span className={isOnline}></span>
            <span> {user.username} </span>
          </td>
          <td> {user.games} </td>
          <td> {user.win} </td>
          <td> {user.loose} </td>
          <td><button className="btn btn-default" disabled={isBtnDisabled}
              onClick={() => dispatch(sendInvitation(payload))}> Play </button></td>
        </tr>
      )
    });

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Username</th>
                <th>Games</th>
                <th>Won</th>
                <th>Lost</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {list}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    users: state.users.users,
    currentUser: state.auth.user,
    notification: state.notification
  };
}

export default connect(mapStateToProps)(Users);
