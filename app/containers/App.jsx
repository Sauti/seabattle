'use strict';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Notification from 'components/Notification'
import Profile from 'components/Profile'

class App extends Component {
  render() {
    return (
      <div className="container">
        <Notification />
        <Profile />
        <div className="col-md-9 col-sm-8 col-xs-12">
          {this.props.children}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

export default connect(mapStateToProps)(App);
