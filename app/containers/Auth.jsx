'use strict';
import React, { Component, PropTypes} from 'react';
import { connect } from 'react-redux'
import style from 'styles/components/auth'
import { login, register, authFail, removeError } from 'actions/AuthActions'

const initialState = {
  isLoginFormVisible: true
};

class Auth extends Component {
  constructor (props) {
    super(props);
    this.state = initialState;
    this.toggleForm = this.toggleForm.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.update = this.update.bind(this);
  }

  toggleForm () {
    this.setState({
      isLoginFormVisible: !this.state.isLoginFormVisible
    });
  }

  update () {
    const { auth, dispatch } = this.props;
    if (auth.error) {
      dispatch(removeError());
    }
  }

  handleSubmit () {
    const { dispatch, auth } = this.props;
    const credentials = {
      username : this.refs.username.value,
      password : this.refs.password.value
    };

    if (!credentials.username) {
      dispatch(authFail('Username is required'));
    }

    if (!credentials.password) {
      dispatch(authFail('Password is required'));
    }

    if (credentials.username && credentials.password && !auth.error) {
      this.state.isLoginFormVisible
        ? dispatch(login(credentials))
        : dispatch(register(credentials))
    }
  }

  render () {
    const { auth } = this.props;
    const errorStyle = auth.error ? {display: 'block'} : {display: 'none'};
    const submitBtnText = this.state.isLoginFormVisible ? 'Login' : 'Register';

    return (
      <div className="col-md-4 col-sm-6 col-xs-12 col-md-offset-4 col-sm-offset-3">
        <div className="panel panel-default">
          <div className="panel-heading">
            <button type="button" onClick={this.toggleForm} className="btn btn-default auth-button"
                    disabled={this.state.isLoginFormVisible}>Login</button>
            <button type="button" onClick={this.toggleForm} className="btn btn-default auth-button"
                    disabled={!this.state.isLoginFormVisible}>Register</button>
          </div>
          <div className="panel-body">
            <div>
              <div className="alert alert-danger" style={errorStyle}> {auth.error} </div>
              <div className="form-group">
                <input type="text" ref="username" className="form-control" placeholder="Username"
                  onChange={this.update}/>
              </div>
              <div className="form-group">
                <input type="password" ref="password" className="form-control" placeholder="Password"
                  onChange={this.update}/>
              </div>
              <div className="form-group text-center">
                <button type="submit" onClick={this.handleSubmit} className="btn btn-success">{submitBtnText}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

export default connect(mapStateToProps)(Auth);
