'use strict';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Field from 'components/Field'
import Notification from 'components/Notification'
import { shoot, endGame } from 'actions/GameActions'

class Game extends Component {
  constructor (props) {
    super(props);
    this.onCellClick = this.onCellClick.bind(this);
  }

  componentWillUnmount () {
    this.props.dispatch(endGame());
  }

  onCellClick (e) {
    const { x, y } = e.target.dataset;
    const { dispatch, game } = this.props;
    const payload = {id: game.id, x: +x, y: +y};
    dispatch(shoot(payload));
  }

  render () {
    const { game, currentUserId } = this.props;
    const turn = currentUserId === game.turn;
    const turnInfo = turn
      ? <h3>Your turn</h3>
      : <h3>Your opponent's turn</h3>;

    return (
      <div className="container">
        <Notification />
        <div className="text-center"> {turnInfo} </div>
        <div className="col-md-2 col-sm-1 hide-xs"></div>
        <Field grid={game.myField}/>
        { turn
          ? <Field grid={game.opponentField} onCellClick={this.onCellClick}/>
          : <Field grid={game.opponentField}/>
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    game: state.game,
    currentUserId: state.auth.user._id
  };
}

export default connect(mapStateToProps)(Game);
