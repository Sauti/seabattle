'use strict';
import React, { Component } from 'react';
import style from 'styles/main';

export default class Layout extends Component {
  render () {
    return (
      <div className="main-layout">
        {this.props.children}
      </div>
    );
  }
}
