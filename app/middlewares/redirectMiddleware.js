let _store;
export const configureRedirectMiddleware = (store) => {
  _store = store;
};

export const requireAuth = (nextState, replace, callback) => {
  const { auth: { isAuthenticated }} = _store.getState();
  if (!isAuthenticated) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    });
  }
  callback();
};

export const redirectIfAuthenticated = (nextState, replace, callback) => {
  const { auth: { isAuthenticated }} = _store.getState();
  if (isAuthenticated) {
    replace({
      pathname: '/'
    });
  }
  callback();
};

export const redirectIfNotYourGame = (nextState, replace, callback) => {
  requireAuth(nextState, replace, () => {
    const { game } = _store.getState();
    if (game.id !== nextState.params.id) {
      replace({
        pathname: '/'
      })
    }
  });
  callback();
};
