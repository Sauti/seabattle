'use strict';
import {AUTH_SUCCESS, AUTH_FAIL, REMOVE_ERROR,
        LOGOUT_SUCCESS, CHECKED_STATUS} from '../actions/AuthActions'

const defaultState = {
  isAuthenticated: false,
  user: null,
  error: null
};

export default (authState = defaultState, action) => {
  switch (action.type) {
    case AUTH_SUCCESS:
      return Object.assign({}, authState, {
        isAuthenticated: true,
        user: action.user,
        error: null
      });

    case AUTH_FAIL:
      return Object.assign({}, authState, {
        isAuthenticated: false,
        error: action.error
      });

    case REMOVE_ERROR:
    case LOGOUT_SUCCESS:
      return Object.assign({}, defaultState);
      return Object.assign({}, defaultState);

    default: return authState;
  }
}
