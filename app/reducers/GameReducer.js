'use strict';
import { HANDLE_GAME_START, INIT_FIELDS, HANDLE_SHOT, END_GAME } from '../actions/GameActions'

const defaultState = {
  id: null,
  myField: [],
  opponentField: [],
  turn: null
};

export default (gameState = defaultState, action) => {
  switch (action.type) {
    case HANDLE_GAME_START:
      return Object.assign({}, gameState, {
        id: action.room,
        turn: action.turn
      });

    case INIT_FIELDS:
      return Object.assign({}, gameState, {
        myField: action.fields[0],
        opponentField: action.fields[1]
      });

    case HANDLE_SHOT:
      const { myField, opponentField, turn } = action;
      const newState = myField ? { turn, myField } : { turn,  opponentField };

      return Object.assign({}, gameState, newState);

    case END_GAME:
      return Object.assign({}, defaultState);

    default: return gameState;
  }
}
