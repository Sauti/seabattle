'use strict';
import { SEND_INVITATION, GET_INVITATION, GET_REJECTION,
        REJECT_INVITATION, ACCEPT_INVITATION, SHOW_NOTIFICATION,CLOSE_NOTIFICATION,
        rejectInvitation, acceptInvitation, closeNotification,
        closeNotificationAndRedirect } from '../actions/GameActions'

const defaultState = {
  isWaitingForResponse: false,
  message: '',
  sender: null,
  onClickHandler: []
};

export default (notificationState = defaultState, action) => {
  switch (action.type) {
    case SHOW_NOTIFICATION:
      return Object.assign({}, notificationState, {
        message: action.message,
        onClickHandler: [
          { handler: action.handler, btnText: 'OK' }
        ]
      });

    case SEND_INVITATION:
      return Object.assign({}, notificationState, {
        isWaitingForResponse: true
      });

    case GET_INVITATION:
      return Object.assign({}, notificationState, {
        message: 'You got an invitation. Do you want to play?',
        sender: action.id,
        onClickHandler: [
          { handler: acceptInvitation, btnText: 'Accept' },
          { handler: rejectInvitation, btnText: 'Reject' }
        ]
      });

    case REJECT_INVITATION:
    case ACCEPT_INVITATION:
    case CLOSE_NOTIFICATION:
      return Object.assign({}, defaultState);

    case GET_REJECTION:
      return Object.assign({}, notificationState, {
        isWaitingForResponse: false,
        message: 'Your invitation was rejected.',
        onClickHandler: [
          { handler: closeNotification, btnText: 'OK' }
        ]
      });

    default: return defaultState;
  }
}
