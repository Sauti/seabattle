'use strict';
import {GET_USERS_SUCCESS, GET_USERS_FAIL} from '../actions/UserActions'

const defaultState = {
  users: [],
  error: null
};

export default (usersState = defaultState, action) => {
  switch (action.type) {
    case GET_USERS_SUCCESS:
      return Object.assign({}, usersState, {
        users: action.users,
        error: null
      });

    case GET_USERS_FAIL:
      return Object.assign({}, usersState, {
        error: action.error
      });

    default: return usersState;
  }
}
