'use strict';
import { combineReducers } from 'redux';
import auth from 'reducers/AuthReducer';
import game from 'reducers/GameReducer';
import notification from 'reducers/NotificationReducer';
import users from 'reducers/UsersReducer';
import { routerReducer as routing } from 'react-router-redux';

const rootReducer = combineReducers({
  auth,
  game,
  notification,
  routing,
  users
});

export default rootReducer;
