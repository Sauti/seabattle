import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';
import { configureRedirectMiddleware, requireAuth, redirectIfAuthenticated,
        redirectIfNotYourGame} from 'middlewares/redirectMiddleware';

import Layout from 'containers/Layout';
import Auth from 'containers/Auth';
import App from 'containers/App';
import Game from 'containers/Game';

import Users from 'components/Users';

export default (store) => {
  configureRedirectMiddleware(store);

  return (
    <Route path='/' component={Layout}>
      <Route component={App} onEnter={requireAuth}>
        <IndexRoute component={Users}/>
      </Route>
      <Route path='login' component={Auth} onEnter={redirectIfAuthenticated} />
      <Route path='game'>
        <IndexRedirect to='/' />
        <Route path=':id' component={Game} onEnter={redirectIfNotYourGame} />
      </Route>
    </Route>
  );
};
