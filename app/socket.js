import { getInvitation, getRejection, handleGameStart, initFields, handleShot } from 'actions/GameActions'
import io from 'socket.io-client';
const socket = io('');

export function configureSocket (store) {
  if (store.getState().auth.isAuthenticated) {
    socket.emit('login', store.getState().auth.user._id);
  }

  socket.on('get invitation', id => {
    store.dispatch(getInvitation(id))
  });

  socket.on('get rejection', () => {
    store.dispatch(getRejection())
  });

  socket.on('start game', payload => {
    store.dispatch(handleGameStart(payload));
  });

  socket.on('init fields', fields => {
    store.dispatch(initFields(fields));
  });

  socket.on('shot result', result => {
    store.dispatch(handleShot(result));
  })
}

export default socket;
