export const env = process.env.NODE_ENV || 'development';

export const mongodb = {
  url: 'mongodb://127.0.0.1:27017/SeaBattle',
  poolSize: 5
};

export const session = {
  secret: 'seabattle',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 604800000
  }
};
