import express from 'express';
import passport from 'passport';
import session from 'express-session';
import bodyParser from 'body-parser';
import path from 'path';
import logger from 'morgan'
import flash from 'express-flash';
import methodOverride from 'method-override';
import { session as sessionConfig } from './config';

export default (app) => {
  app.set('port', (process.env.PORT || 3000));
  app.set('view cache', false);
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(methodOverride());
  app.use(express.static(path.join(__dirname, '../..', 'public')));
  app.use(session(sessionConfig));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(flash());
};
