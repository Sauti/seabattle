import { mongodb as config } from './config';
import mongoose from 'mongoose'

export default () => {
  const { url, poolSize } = config;

  const options = {
    server: { poolSize },
    replset: { keepAlive: 1 }
  };

  var db = mongoose.connection;

  db.on('error', (err) => {
    console.error('Error occurred while establishing new MongoDB connection. ' + err);
  });

  db.on('connected', () => {
    console.log('Successfully connected to MongoDB on "' + url + '" with pool size: ' + poolSize);
  });

  db.on('disconnected', () => {
    console.error('Connection lost or not established with MongoDB on "' + url + '" Reconnecting in 5s.');
    setTimeout(connect, 5000);
  });

  function connect () {
    console.log('Try to establish MongoDB connection on "' + url + '" with pool size: ' + poolSize);
    mongoose.connect(url, options);
  }

  connect();
};
