import passport from 'passport';
import LocalStrategy from 'passport-local';
import User from '../models/user';
import bcrypt from 'bcrypt';

export default () => {
  passport.use(new LocalStrategy((username, password, done) => {
    User.findOne({username: username}, '+password', (err, user) => {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done(null, null, {message: 'Incorrect username.'});
      }

      bcrypt.compare(password, user.password, function (error, isValid) {
        if (error) {
          return error;
        }

        if (!isValid) {
          return done(null, null, {message: 'Incorrect password.'})
        }

        return done(null, user)
      })
    })
  }));

  passport.serializeUser((user, done) => {
    done(null, user && user._id);
  });

  passport.deserializeUser((id, done) => {
    User.findOne({_id: id}, '+password', (err, user) => {
      if (err) {
        return done(err);
      }

      return done(null, user);
    });
  });
};
