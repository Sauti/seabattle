import User from '../models/user';
import passport from 'passport';
import bcrypt from 'bcrypt';

export function register (req, res, next) {
  bcrypt.hash(req.body.password, 10, (error, hash) => {
    req.body.password = hash;

    User.create(req.body, (err, user) => {
      if (err) {
        if (err.code === 11000) {
          return res.status(400).send({message: 'This username is already in use.'});
        } else {
          return res.status(400).send({message: err.message});
        }
      }

      req.logIn(user, (err) => {
        res.send({user: deletePassword(req.user)});
      })
    });
  });
}

export function login (req, res, next) {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.status(400).send({message: info.message});
    }

    req.logIn(user, (err) => {
      if (err) {
        return next(err);
      }

      res.send({user: deletePassword(req.user)});
    })
  })(req, res, next)
}

export function logout (req, res, next) {
  req.logOut();
  res.end();
}

export function isAuthenticated (req, res, next) {
  res.send(req.user ? {user: deletePassword(req.user)} : null);
}

function deletePassword (user) {
  var _user = JSON.parse(JSON.stringify(user));
  delete _user.password;
  return _user;
}
