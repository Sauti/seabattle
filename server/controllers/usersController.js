import User from '../models/user';
import { connections } from '../socket'
var ObjectId = require('mongoose').Types.ObjectId;

export function getAll (req, res, next) {
  User.find({_id: {$ne: ObjectId(req.user._id)}}, (error, users) => {
    if (error) {
      return res.send({ error });
    }

    users = JSON.parse(JSON.stringify(users));
    for (let user of users) {
      for (let [userId, socketId] of connections) {
        if (userId === user._id) {
          user.isOnline = true;
        }
      }
    }

    return res.send({ users })
  })
}

export function updateGameInfo (id, status) {
  if (status !== 'win' && status !== 'loose') {
    throw 'Unknown game status';
  }

  User.findByIdAndUpdate(id, {$inc: {[status]: 1, games: 1}}, (error, users) => {
    if (error) {
      throw error;
    }
  })
}
