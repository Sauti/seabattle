'use strict';
import _ from 'lodash';

export default class Field {
  static get CELLTYPE () {
    return {
      EMPTY: 0,
      SHIP: 1,
      WRAP: '.',
      HIT: '*',
      MISS: '-'
    }
  };

  constructor (size = 10) {
    this.size = size;
    this.grid = Field.generateGrid(size);
    this.availableCells = _.flatten(Field.generateGrid(size, true));
    this.ships = [];
  };

  toString () {
    return this.grid.reduce((rowStr, row) => {
      return rowStr += row.reduce((cellStr, cell) => {
          return cellStr += cell + ' ';
        }, '') + '\n';
    }, '');
  };

  removeAvailableCell (cell) {
    let index = -1;
    for (let i = 0; i < this.availableCells.length; i++) {
      if (this.availableCells[i][0] == cell[0] && this.availableCells[i][1] == cell[1]) {
        index = i;
        break;
      }
    }

    if (index > -1) {
      this.availableCells.splice(index, 1);
    }
  }

  static generateGrid (size, isFull) {
    if (isFull) {
      return new Array(size).fill(0).map((row, y) => {
        return new Array(size).fill(0).map((cell, x) => {
          return [x, y];
        })
      });
    } else {
      return new Array(size).fill(0).map(() => {
        return new Array(size).fill(Field.CELLTYPE.EMPTY);
      });
    }
  };

  canPlaceShip (ship) {
    for (let i = 0; i < ship.coords.length; i++) {
      let cell = ship.coords[i];

      if (!this.grid[cell.y]) {
        return false;
      }

      if (this.grid[cell.y][cell.x] !== Field.CELLTYPE.EMPTY) {
        return false;
      }
    }

    return true;
  };

  placeShip (ship) {
    let shipArea = ship.wrapper.concat(ship.coords);

    shipArea.forEach(cell => {
      this.grid[cell.y][cell.x] = cell.type;
      this.removeAvailableCell([cell.x, cell.y]);
    });
  };

  placeShips (ships) {
    ships.forEach(ship => {
      do {
        ship.move(this.availableCells, this.size);
      } while (!this.canPlaceShip(ship));

      this.placeShip(ship);
      this.ships.push(ship);
    });
  };

  handleShot (x, y) {
    if (this.grid[y][x] !== Field.CELLTYPE.SHIP) {
      this.grid[y][x] = Field.CELLTYPE.MISS;

      return [{x, y, type: Field.CELLTYPE.MISS}]
    } else {
      let result = [];

      this.ships.forEach(ship => {
        if (_.find(ship.coords, { x: x, y: y })) {
          ship.isAlive--;

          if (ship.isAlive) {
            result = [{ x, y, type: Field.CELLTYPE.HIT }]
          } else {
            ship.coords.map(cell => cell.type = Field.CELLTYPE.HIT);
            result = ship.coords.concat(ship.wrapper);
            _.remove(this.ships, ship);
          }
        }
      });

      return result;
    }
  }
}
