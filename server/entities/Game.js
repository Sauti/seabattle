'use strict';
import User from './User';
import Field from './Field';

export default class Game {
  constructor (roomId, user1Id, user2Id) {
    this.id = roomId;
    this.user1 = new User(user1Id);
    this.user2 = new User(user2Id);
    this.turn = this.user1;
  }

  shoot (x, y) {
    const targetUser = this.turn == this.user1 ? this.user2 : this.user1;
    let cells = this.turn.shoot(targetUser, x, y);
    this.turn = cells[0].type === Field.CELLTYPE.MISS ? targetUser : this.turn;
    const usersStatus = this.getUsersStatus();

    const shotResult = { cells, targetUser: targetUser.id, turn: this.turn.id };

    return usersStatus ? {...shotResult, ...usersStatus} : shotResult;
  };

  getUsersStatus () {
    const user1Ships = this.user1.getShips().length;
    const user2Ships = this.user2.getShips().length;

    if (!user1Ships || !user2Ships) {
      return user1Ships
        ? { winner: this.user1.id, looser: this.user2.id }
        : { winner: this.user2.id, looser: this.user1.id }
    }
  }
}
