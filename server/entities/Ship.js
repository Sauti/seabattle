'use strict';
import Field from './Field';
import _ from 'lodash';

export default class Ship {
  static get WRAPPER () {
    return [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]];
  }

  constructor (length) {
    this.len = length;
    this.isAlive = length;
  }

  move (availableCells, fieldSize) {
    this.coords = [];
    this.wrapper = [];
    this.generateCoordinates(availableCells, fieldSize);
    this.generateWrapper(availableCells, fieldSize);
  }

  generateCoordinates (availableCells, fieldSize) {
    var randomCell = Math.floor(Math.random(0, availableCells.length) * availableCells.length);
    this.x = availableCells[randomCell][0];
    this.y = availableCells[randomCell][1];
    this.modeX = Math.round(Math.random());
    this.modeY = +!this.modeX;

    for (let part = 0; part < this.len; part++) {
      let x = this.x + this.modeX * part;
      let y = this.y + this.modeY * part;

      if (x < 0 || x > fieldSize - 1 || y < 0 || y > fieldSize - 1) {
        this.move(availableCells, fieldSize);
      }

      this.coords.push({x: x, y: y, type: Field.CELLTYPE.SHIP});
    }
  }

  generateWrapper (availableCells, fieldSize) {
    for (let cell = 0; cell < this.len; cell++) {
      for (let i = 0; i < 8; i++) {
        let wrapX = this.coords[cell].x + Ship.WRAPPER[i][0];
        let wrapY = this.coords[cell].y + Ship.WRAPPER[i][1];

        if (wrapX < -1 || wrapX > fieldSize || wrapY < -1 || wrapY > fieldSize) {
          this.move(availableCells, fieldSize);
        }

        let cellExistsInCoords = _.find(this.coords, {x: wrapX, y: wrapY});
        let cellExistsInWrapper = _.find(this.wrapper, {x: wrapX, y: wrapY});

        if (!cellExistsInCoords && !cellExistsInWrapper) {
          if (wrapX > -1 && wrapX < fieldSize && wrapY > -1 && wrapY < fieldSize){
            this.wrapper.push({x: wrapX, y: wrapY, type: Field.CELLTYPE.WRAP});
          }
        }
      }
    }
  }
}
