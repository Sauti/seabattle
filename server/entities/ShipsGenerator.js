'use strict';
import Ship from './Ship';

export default class ShipsGenerator {
  constructor () {
    var ships = [];
    var shipParams = new Map([[4, 1], [3, 2], [2, 3], [1, 4]]);

    for (let [length, quantity] of shipParams) {
      for (let i = 0; i < quantity; i++) {
        ships.push(new Ship(length));
      }
    }

    return ships;
  }
}
