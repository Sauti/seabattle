'use strict';
import Field from './Field';
import ShipsGenerator from './ShipsGenerator';

export default class User {
  constructor (id) {
    this.id = id;
    this.field = new Field();
    this.field.placeShips(new ShipsGenerator());

    this.opponentField = new Field();
  }

  getFields () {
    return [this.field.grid, this.opponentField.grid];
  }

  getShips () {
    let ships = [];
    this.field.ships.forEach(ship => ships.push(ship.len));

    return ships;
  }

  shoot (user, x, y) {
    let cells = user.handleShot(x, y);
    for (let cell of cells) {
      this.opponentField.grid[cell.y][cell.x] = cell.type;
    }

    return cells;
  }

  handleShot (x, y) {
    return this.field.handleShot(x, y);
  }
}
