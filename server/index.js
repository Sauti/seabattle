import express from 'express';
import webpack from 'webpack';
import { env } from './config/config';
import mongoConfig from './config/mongo';
import passportConfig from './config/passport';
import expressConfig from './config/express';
import webpackDevConfig from '../webpack/webpack.config.dev-client';
import api from './routes/api';
import App from '../public/assets/server';
import socketEvents from './socket';
const app = express();

mongoConfig();
passportConfig();

if (env === 'development') {
  const compiler = webpack(webpackDevConfig);
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: webpackDevConfig.output.publicPath
  }));

  app.use(require('webpack-hot-middleware')(compiler));
}

expressConfig(app);

app.use('/api', api);
app.get('*', App);

const server = app.listen(app.get('port'));

const io = require('socket.io')(server);
socketEvents(io);
