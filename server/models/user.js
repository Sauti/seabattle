import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const user = new Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true, select: false },
  games: { type: Number, default: 0, required: true },
  win: { type: Number, default: 0, required: true },
  loose: { type: Number, default: 0, required: true }
});

user.set('autoIndex', true);

export default mongoose.model('User', user);
