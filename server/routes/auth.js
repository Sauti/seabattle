import express from 'express';
const router = express.Router();

import * as auth from '../controllers/authController';

router.route('/register')
  .post(auth.register);

router.route('/login')
  .post(auth.login);

router.route('/logout')
  .get(auth.logout);

router.route('/isAuthenticated')
  .get(auth.isAuthenticated);

export default router;
