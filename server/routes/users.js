import express from 'express';
const router = express.Router();

import * as users from '../controllers/usersController';

router.route('/')
  .get(users.getAll);

export default router;
