import Game from './entities/Game';
import { updateGameInfo } from './controllers/usersController.js'
export let connections = new Map();
let games = new Map();

export default function (io) {
  io.on('connection', socket => {
    socket.on('login', userId => {
      connections.set(userId, socket.id);
    });

    socket.on('logout', () => {
      connections.delete(getUserIdBySocketId(socket.id));
    });

    socket.on('disconnect', function() {
      connections.delete(getUserIdBySocketId(socket.id));
    });

    socket.on('send invitation', users => {
      const targetUser = connections.get(users.receiver);
      targetUser
        ? socket.broadcast.to(targetUser).emit('get invitation', users.sender)
        : socket.emit('get rejection');
    });

    socket.on('reject invitation', userId => {
      socket.broadcast.to(connections.get(userId)).emit('get rejection');
    });

    socket.on('accept invitation', users => {
      const room = Math.random().toString(36).substr(2, 5);
      const game = new Game(room, users.sender, users.receiver);

      games.set(room, game);

      socket.join(room);
      io.sockets.connected[connections.get(users.sender)].join(room);
      io.to(room).emit('start game', {room, turn: game.turn.id});

      socket.emit('init fields', game.user2.getFields());
      socket.broadcast.to(connections.get(users.sender)).emit('init fields', game.user1.getFields());
    });

    socket.on('shoot', payload => {
      const { id, x, y } = payload;
      const result = games.get(id).shoot(x, y);
      const { winner, looser } = result;

      io.to(id).emit('shot result', result);

      if (winner) {
        updateGameInfo(winner, 'win');
        updateGameInfo(looser, 'loose');
        games.delete(id);
      }
    })
  });
}

function getUserIdBySocketId (socketId) {
  for (let [userId, socket] of connections) {
    if (socket === socketId) {
      return userId;
    }
  }
}
