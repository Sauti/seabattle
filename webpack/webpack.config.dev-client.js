var path = require('path');
var webpack = require('webpack');
var assetsPath = path.join(__dirname, '..', 'public', 'assets');
var hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

var commonLoaders = [
  {
    test: /\.js$|\.jsx$/,
    loader: 'babel-loader',
    query: {
      "presets": ["react-hmre", "es2015", "react", "stage-0"],
      "plugins": ["transform-decorators-legacy"]
    },
    include: path.join(__dirname, '..', 'app'),
    exclude: path.join(__dirname, '..', 'node_modules')
  },
  {
    test: /\.(woff|woff2)$/,
    loader: 'url',
    query: {
      name: '[name].[ext]',
      limit: 10000
    }
  },
  { test: /\.(eot|ttf|svg|gif|png)$/,
    loader: "file-loader",
    query: {
      name: '[name].[ext]',
      limit: 10000
    }
  },
  { test: /\.html$/, loader: 'html-loader' },
  { test: /\.less$/, loader: 'style-loader!css-loader!less-loader' }
];

module.exports = {
  devtool: 'eval',
  name: 'browser',
  context: path.join(__dirname, '..', 'app'),
  entry: {
    app: ['./client', hotMiddlewareScript]
  },
  output: {
    path: assetsPath,
    filename: '[name].js',
    publicPath: '/assets/'
  },
  module: {
    loaders: commonLoaders
  },
  resolve: {
    root: [path.join(__dirname, '..', 'app')],
    extensions: ['', '.js', '.jsx', '.css', '.less']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      __DEVCLIENT__: true,
      __DEVSERVER__: false
    })
  ]
};
