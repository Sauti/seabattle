var path = require('path');
var webpack = require('webpack');
var assetsPath = path.join(__dirname, '..', 'public', 'assets');

var commonLoaders = [
  {
    test: /\.js$|\.jsx$/,
    loader: 'babel-loader',
    query: {
      "presets": ["es2015", "react", "stage-0"],
      "plugins": ["transform-decorators-legacy"]
    },
    include: path.join(__dirname, '..', 'app'),
    exclude: path.join(__dirname, '..', 'node_modules')
  },
  { test: /\.json$/, loader: "json-loader" },
  {
    test: /\.(woff|woff2)$/,
    loader: 'url',
    query: {
      name: '[name].[ext]',
      limit: 10000
    }
  },
  {
    test: /\.(eot|ttf|svg|gif|png)$/,
    loader: "file-loader",
    query: {
      name: '[name].[ext]',
      limit: 10000
    }
  },
  { test: /\.html$/, loader: 'html-loader' },
  { test: /\.less$/, loader: 'css-loader!less-loader' }
];

module.exports = {
  name: "server-side rendering",
  context: path.join(__dirname, "..", "app"),
  entry: {
    server: "./server"
  },
  target: "node",
  output: {
    path: assetsPath,
    filename: "server.js",
    publicPath: "/assets/",
    libraryTarget: "commonjs2"
  },
  module: {
    loaders: commonLoaders
  },
  resolve: {
    root: [path.join(__dirname, '..', 'app')],
    extensions: ['', '.js', '.jsx', '.css', '.less']
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEVCLIENT__: false,
      __DEVSERVER__: true
    })
  ]
};
